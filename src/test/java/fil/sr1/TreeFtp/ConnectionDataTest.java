package fil.sr1.TreeFtp;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * @author Kevin Nguyen
 * Test the methods from the class ConnectionData.
 */
public class ConnectionDataTest {

	@Test
	public void testPortData() {

		int test = ConnectionData.portData("227 Entering Passive Mode (91,189,88,142,137,248).");
		int test2 = ConnectionData.portData("227 Entering Passive Mode (91,19,108,142,212,59).");

		assertEquals(35320, test);
		assertNotEquals(21854, test);
		assertEquals(54331, test2);
		assertNotEquals(8743, test2);
	}

	@Test
	public void testIpData() {

		String test = ConnectionData.ipData("227 Entering Passive Mode (91,189,88,152,173,4).");
		String test2 = ConnectionData.ipData("227 Entering Passive Mode (90,130,70,73,96,147).");

		assertEquals("91.189.88.152", test);
		assertNotEquals("91,189,88,152", test);
		assertNotEquals("45.19.98.36", test);
		assertEquals("90.130.70.73", test2);
		assertNotEquals("125.105.178.46", test2);
	}

}
