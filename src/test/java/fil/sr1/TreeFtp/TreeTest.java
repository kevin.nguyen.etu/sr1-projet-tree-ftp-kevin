package fil.sr1.TreeFtp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * @author Kevin Nguyen
 * Test the methods from the class Tree.
 */
public class TreeTest {

	@Test
	public void testgatherData() {
		String[] test = Tree.gatherData("drwxrwxr-x   3 997   997   4096 Aug 08  2019 bionic");
		String[] test2 = Tree.gatherData("lrwxrwxr-x   3 997   997   476 Aug 08  2005 20.10 -> groovy");
		String[] test3 = Tree.gatherData("-rwxrwxr-x   3 907   994   476 Aug 08  2015 robot");

		assertEquals("d", test[0]);
		assertNotEquals("l", test[0]);
		assertNotEquals("-", test[0]);
		assertEquals("bionic", test[1]);
		assertNotEquals("azdionic", test[1]);
		assertEquals("l", test2[0]);
		assertEquals("20.10 -> groovy", test2[1]);
		assertNotEquals("20.10 ->groovy", test2[1]);
		assertEquals("-", test3[0]);
		assertEquals("robot", test3[1]);
	}

}
