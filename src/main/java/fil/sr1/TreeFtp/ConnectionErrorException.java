package fil.sr1.TreeFtp;

/**
 * 
 * @author Kevin Nguyen
 *
 */
public class ConnectionErrorException extends Exception {
	public ConnectionErrorException(String message) {
		super(message);
	}
}
