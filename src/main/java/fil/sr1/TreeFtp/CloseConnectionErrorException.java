package fil.sr1.TreeFtp;

/**
 * 
 * @author Kevin Nguyen
 *
 */
public class CloseConnectionErrorException extends Exception {

	public CloseConnectionErrorException(String message) {
		super(message);
	}
}
