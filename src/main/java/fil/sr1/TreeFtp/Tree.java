package fil.sr1.TreeFtp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Kevin Nguyen
 * Display the tree structure of a ftp server.
 */
public class Tree {

	/**
	 * the tree method that will display the tree structure of a ftp server
	 * 
	 * @param connection  the connection to the server, from which to the ftp tree
	 * @param indentation the amount of indentation to display file as a tree,
	 *                    depending of how deep we are in the tree structure of the
	 *                    ftp server
	 * @param depth       represent the number of imbricated folders, how deep we
	 *                    want to display the tree structure of the ftp server.
	 * @throws ConnectionErrorException      if it fail to do a command PASV or
	 *                                       LIST.
	 * @throws CloseConnectionErrorException if it fail to close the connection for
	 *                                       the data.
	 */
	public static void tree(ConnectionCommand connection, int indentation, int depth)
			throws ConnectionErrorException, CloseConnectionErrorException {
		try {
			ConnectionData connection2 = connection.commandPasv();
			connection.command("LIST");
			connection.reader.readLine();
			displayData(connection, connection2, indentation, depth);
		} catch (IOException e) {
			throw new ConnectionErrorException("Connexion error.");
		}
	}

	/**
	 * gather the type and name the file
	 * 
	 * @param line from which we want to extract the data
	 * @return an array containing only the type and name of the file, data[0]
	 *         represent the type and data[1] represent the name of the file
	 */
	public static String[] gatherData(String line) {
		String[] splittedLine = line.split(" ");
		String type = Character.toString(splittedLine[0].charAt(0));
		String file = "";
		if (type.equals("l")) {
			file = splittedLine[splittedLine.length - 3];
			file += " -> ";
		}
		file += splittedLine[splittedLine.length - 1];
		String[] data = { type, file };
		return data;
	}

	/**
	 * gather all the descriptions of files of a directory
	 * 
	 * @param connectionData the connection from which extract the directory
	 *                       information
	 * @return a list which each element describe a file of the directory by an
	 *         array
	 * @throws ConnectionErrorException if it fail to gather data which mean there
	 *                                  is connection error
	 */
	private static ArrayList<String[]> gatherDirectoryData(ConnectionData connectionData)
			throws ConnectionErrorException {
		try {
			ArrayList<String[]> listData = new ArrayList<String[]>();
			String line = connectionData.getReader().readLine();
			while (line != null) {
				String[] data = gatherData(line);
				listData.add(data);
				line = connectionData.getReader().readLine();
			}
			return listData;
		} catch (IOException e) {
			throw new ConnectionErrorException("Error gathering data.");
		}
	}

	/**
	 * display the name of all the files of a directory and redo a tree for each
	 * directory in it
	 * 
	 * @param connectionCmd  the connection to the ftp server from which we will
	 *                       send ftp command to gather data
	 * @param connectionData the connection from which we will get all the
	 *                       descriptions of files in the directory
	 * @param indentation    the amount of indentation to display file as a tree,
	 *                       depending of how deep we are in the tree structure of
	 *                       the ftp server
	 * @param depth          represent the number of imbricated folders, how deep we
	 *                       want to display the tree structure of the ftp server.
	 * @throws ConnectionErrorException      if it fail to do a command CDUP or CWD.
	 * @throws CloseConnectionErrorException if it fail to close the connection for
	 *                                       the data.
	 */
	private static void displayData(ConnectionCommand connectionCmd, ConnectionData connectionData, int indentation,
			int depth) throws ConnectionErrorException, CloseConnectionErrorException {
		ArrayList<String[]> listData = gatherDirectoryData(connectionData);
		connectionData.close();
		for (String[] file : listData) {
			displayIndentation(indentation);
			System.out.println(file[1]);
			if (file[0].equals("d")) {
				switchDirectory(connectionCmd, indentation, depth, file);
			}
		}
	}

	/**
	 * the number of indentation to do before displaying the file name respecting
	 * the tree structure
	 * 
	 * @param indentation the amount of indentation to display file as a tree,
	 *                    depending of how deep we are in the tree structure of the
	 *                    ftp server
	 */
	private static void displayIndentation(int indentation) {
		for (int i = 0; i < indentation; i++) {
			System.out.print("\t");
		}
	}

	/**
	 * Switch to an another directory
	 * 
	 * @param connectionCmd the connection to the ftp server from which we will send
	 *                      ftp command to gather data
	 * @param indentation   the amount of indentation to display file as a tree,
	 *                      depending of how deep we are in the tree structure of
	 *                      the ftp server
	 * @param depth         represent the number of imbricated folders, how deep we
	 *                      want to display the tree structure of the ftp server (-1 to display the tree
	 * structure of the entire server).
	 * @param file          the description of a file of type directory in the
	 *                      directory
	 */
	private static void switchDirectory(ConnectionCommand connectionCmd, int indentation, int depth, String[] file) {
		if (indentation < depth || depth == -1) {
			try {
				// Il y a un pour moi avec ces 2 dossiers, si je tente d'entré dans ces dossiers, ma connexion s'interrompt
				// instantanément, alors que d'autre dossiers ayant les même droits n'ont pas ce problème.
				// J'ai donc décider de les ignorer.
				if (!(file[1].equals("tmp") || file[1].equals("assistance"))) {
					String message = connectionCmd.command("CWD " + file[1]);
					if (!(message.contains("Failed"))) {
						tree(connectionCmd, indentation + 1, depth);
						connectionCmd.command("CDUP");
					}
				}
			} catch (ConnectionErrorException e) {
				System.out.println("Erreur connexion.");
			} catch (CloseConnectionErrorException e) {
				System.out.println("Erreur pour la fermeture de la connexion");
			}
		}
	}
}
