package fil.sr1.TreeFtp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Kevin Nguyen
 * Create a connection with everything needed for pushing message or receiving message
 */
public abstract class Connection {

	protected Socket socket;
	protected BufferedReader reader;
	protected PrintWriter printer;

	/**
	 * create a connection with everything needed for push message or receiving
	 * message
	 * 
	 * @param ip   the ip to which to connect
	 * @param port the port to which to connect
	 * @throws ConnectionErrorException if the connection fail
	 */
	public Connection(String ip, int port) throws ConnectionErrorException {
		try {
			this.socket = new Socket(ip, port);
			OutputStream out = socket.getOutputStream();
			this.printer = new PrintWriter(out, true);
			InputStream in = socket.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(in);
			this.reader = new BufferedReader(inputStreamReader);
		} catch (IOException e) {
			throw new ConnectionErrorException("Connexion error.");
		}
	}

	/**
	 * get the printer of the connection to send message
	 * 
	 * @return the printer that will be used to send message
	 */
	public PrintWriter getPrinter() {
		return this.printer;
	}

	/**
	 * get the reader of the connection to receive message
	 * 
	 * @return the reader that will be used to receive message
	 */
	public BufferedReader getReader() {
		return this.reader;
	}

	/**
	 * it will close the connection
	 * 
	 * @throws CloseConnectionErrorException if closing the connection fail
	 */
	public void close() throws CloseConnectionErrorException {
		try {
			this.socket.close();
		} catch (IOException e) {
			throw new CloseConnectionErrorException("Close connection error.");
		}
	}

	/**
	 * the command that will be send to the ftp server
	 * 
	 * @param command the command to send
	 * @return the response from the ftp server
	 */
	public String command(String command) throws ConnectionErrorException {
		this.getPrinter().println(command);
		try {
			return this.getReader().readLine();
		} catch (IOException e) {
			throw new ConnectionErrorException("Connexion error.");
		}
	}
}
