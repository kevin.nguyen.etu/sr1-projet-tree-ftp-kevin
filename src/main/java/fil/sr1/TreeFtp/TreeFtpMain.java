package fil.sr1.TreeFtp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * @author Kevin Nguyen
 *
 */
public class TreeFtpMain {

	public static void main(String[] args) {
		try {
			ConnectionCommand connectionFtpServer = new ConnectionCommand(args[0], 21);
			int depth = -1;
			String user = "anonymous";
			String password = "kikoolol";

			switch (args.length) {
			case 3:
				user = args[1];
				password = args[2];
				break;
			case 4:
				depth = Integer.parseInt(args[3]);
				break;
			}
			connectionFtpServer.connectFtpServer(user, password);
			Tree.tree(connectionFtpServer, 0, depth);
			connectionFtpServer.close();
		} catch (InvalidUserException e) {
			System.out.println("Mauvais utilisateur ou mot de passe.");
		} catch (ConnectionErrorException e) {
			System.out.println("Erreur connexion.");
		} catch (CloseConnectionErrorException e) {
			System.out.println("Erreur pour la fermeture de la connexion.");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Erreur lors de l'éxecution de la commande.");
			System.out.println("Le premier argument (obligatoire) indique le server auquel ce connecter.");
			System.out.println(
					"Le deuxième argument (optionnel) indique l'utilisateur avec lequel ce connecter. Il va obligatoirement de paire avec le 3ème argument.");
			System.out.println(
					"Le deuxième argument (optionnel) indique le mot de passe avec lequel ce connecter. Il va obligatoirement de paire avec le 2ème argument.");
			System.out.println(
					"Le quatrième argument (optionnel) indique la profondeur maximale d'arborescence que l'on souhaite afficher.");
			System.out.println("Exemple d'utilisation :");
			System.out.println("java -jar TreeFtp.jar server utilisateur motdepasse 2(profondeur)");
		}
	}
}
