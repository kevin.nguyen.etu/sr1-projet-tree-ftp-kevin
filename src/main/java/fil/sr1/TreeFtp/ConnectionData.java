package fil.sr1.TreeFtp;

import java.io.IOException;

/**
 * @author Kevin Nguyen
 * Create a connection to receive the data from a ftp server
 */
public class ConnectionData extends Connection {

	/**
	 * Create a connection to receive data from a ftp server
	 * 
	 * @param ip   the ip which to connect to receive the data
	 * @param port the port which to connect to receive the data
	 * @throws ConnectionErrorException if the connection fail
	 */
	public ConnectionData(String ip, int port) throws ConnectionErrorException {
		super(ip, port);
	}

	/**
	 * calculate the port from a PASV command
	 * 
	 * @param commandLine the string from which extract the port
	 * @return the port to connect to receive the data
	 */
	public static int portData(String commandLine) {
		String ipPort = commandLine.substring(commandLine.indexOf("(") + 1, commandLine.indexOf(")"));
		String[] array = ipPort.split(",");
		return (Integer.parseInt(array[array.length - 2]) * 256) + Integer.parseInt(array[array.length - 1]);
	}

	/**
	 * extract the ip from a response of a PASV command
	 * 
	 * @param commandLine the string from which to extract the ip
	 * @return the ip in the form 'xxx.xxx.xxx.xxx'
	 */
	public static String ipData(String commandLine) {
		String ipPort = commandLine.substring(commandLine.indexOf("(") + 1, commandLine.indexOf(")"));
		String[] array = ipPort.split(",");
		String ipdata = "";
		for (int i = 0; i < 4; i++) {
			ipdata = ipdata + array[i] + ".";
		}
		return ipdata.substring(0, ipdata.length() - 1);
	}
}
