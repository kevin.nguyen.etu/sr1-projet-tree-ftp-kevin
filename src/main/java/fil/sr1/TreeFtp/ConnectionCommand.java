package fil.sr1.TreeFtp;

import java.io.IOException;

/**
 * @author Kevin Nguyen
 * Create a connection to receive the response from a ftp server
 */
public class ConnectionCommand extends Connection {

	/**
	 * Create a connection to receive the response from a ftp server
	 * 
	 * @param ip   the ip which to connect to receive the response from the ftp
	 *             server
	 * @param port the port which to connect to receive the response from the ftp
	 *             server
	 * @throws ConnectionErrorException if the connection fail
	 */
	public ConnectionCommand(String ip, int port) throws ConnectionErrorException {
		super(ip, port);
		try {
			this.reader.readLine();
		} catch (IOException e) {
			throw new ConnectionErrorException("Connection error.");
		}
	}

	/**
	 * connect to a ftp server with a user and password
	 * 
	 * @param user     the user used for the login
	 * @param password the password used for the login
	 * @return true if the connection succeed, false otherwise
	 * @throws InvalidUserException if the user or password is invalid
	 * @throws ConnectionErrorException if the connection fail
	 */
	public boolean connectUser(String user, String password) throws InvalidUserException, ConnectionErrorException {
		String response = this.command("USER " + user);
		if (response.startsWith("5")) {
			throw new InvalidUserException("Incorrect user.");
		} else {
			response = this.command("PASS " + password);
			if (response.startsWith("5")) {
				throw new InvalidUserException("Incorrect password.");
			} else {
				return true;
			}
		}
	}

	/**
	 * connect to a ftp server
	 * 
	 * @param user     the user that will be used to connect to the ftp server
	 * @param password the password that will be used to connect to the ftp server
	 * @throws InvalidUserException if the login is invalid
	 * @throws ConnectionErrorException if the connection fail
	 */
	public void connectFtpServer(String user, String password) throws InvalidUserException, ConnectionErrorException {
		this.command("AUTH TLS");
		connectUser(user, password);
		this.command("PWD");
		this.command("TYPE I");
	}

	/**
	 * launch a command PASV to the ftp server to create connection to receive the
	 * data
	 * 
	 * @return the connection to gather data from the ftp server
	 * @throws ConnectionErrorException if the connection fail to establish
	 */
	public ConnectionData commandPasv() throws ConnectionErrorException {
		String commandIpPort = command("PASV");
		int dataPort = ConnectionData.portData(commandIpPort);
		String dataIp = ConnectionData.ipData(commandIpPort);
		return new ConnectionData(dataIp, dataPort);
	}
}
