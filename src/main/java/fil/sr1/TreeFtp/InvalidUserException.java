package fil.sr1.TreeFtp;

/**
 * 
 * @author Kevin Nguyen
 *
 */
public class InvalidUserException extends Exception {
	public InvalidUserException(String message) {
		super(message);
	}
}
