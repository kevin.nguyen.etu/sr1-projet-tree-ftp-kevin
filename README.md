# Projet Application Tree FTP

Kévin Nguyen
18/01/21


## Introduction

Le but de ce projet est de réalisé une commande treeftp qui permet d'afficher
l'arborescence d'un répertoire à distance accessible via le protocole FTP qui
s'inspirera du formalisme de la commande tree de Linux.

## Instruction

Depuis la racine du projet pour générer la javadoc, il faut éxecuter la commande :

mvn javadoc:javadoc

Si jamais la génération de la javadoc échoue, une potentiel solution est de configurer le chemin d'accès a la commande javadoc puis de recommencer la commande pour la javadoc, sur linux :

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

Depuis la racine du projet pour compiler l'application, il faut éxecuter la commande :

mvn package

Depuis la racine du projet pour éxecuter l'application, il faut éxecuter la commande :

java -jar target/TreeFtp-1.0-SNAPSHOT.jar nomduserveur nomutilisateur motdepasse 1

- nomduserveur : argument obligatoire

- nomutilisateur : argument optionnel mais doit obligatoirement être utiliser avec l'argument mot de passe, si utiliser.

- motdepasse : argument optionnel mais doit obligatoirement être utiliser avec l'argument nom d'utilisateur, si utiliser.

- 1 : représente la profondeur maximal qu'on souhaite afficher (pour que la profondeur soit infini il faut mettre -1, argument optionnel mais ne peut être utiliser que si on met un nom d'utilisateur et un mot de passe.

## Architecture

###Polymorhisme :

Classe Connection (Classe abstraite représententant une connexion)

###Catch :

IOException :

On intercepte une erreur quand la connexion n'arrive pas a ce faire ou elle est interrompu soudainement.
Pour la résoudre on fais remonter une exception qui précise l'erreur qui vient de ce produire.

ConnectionErrorException :

Lorsque qu'il y a un problème avec la connexion.
On va alors interrompre l'application, et informer l'utilisateur d'une erreur de connexion.

CloseConnectionErrorException :

Lorsque qu'il y a un problème avec la fermeture de la connexion.
On va alors interrompre l'application, et informer l'utilisateur d'une erreur lors de la tentative de fermeture de la connexion.

InvalidUserException :

Lorsque que l'on tente de ce connecter avec un identifiant et un mot de passe
qui ne fonctionne pas.
On va alors interrompre l'application, et informer l'utilisateur d'une erreur concernant l'identifiant et/ou le mot de passe.

ArrayIndexOutOfBoundsException :

Lorsque l'on lance la commande treeftp mais qu'il les arguments ne corresponde pas a ce qui est demander.
On va alors interrompre l'application, et informer l'utilisateur d'une erreur concernant la saisie de la commande.

###Throw :

ConnectionErrorException :

Permet de remonter une erreur de connexion

CloseConnectionErrorException :

Permet de remonter une erreur de fermeture d'une connexion

InvalidUserException :

Permet de remonter une erreur de login, utilisateur et mot de passe

## Code Samples

Une adresse ip et un port sont passer en paramétre ce qui va ensuite permettre
de créer un canal d'entré (inputstream) pour les réponses envoyer par le serveur
et créer un canal de sortie (outputstream) pour envoyer des messages au serveur.

public Connection(String ip, int port) throws ConnectionErrorException {
		try {
			this.socket = new Socket(ip, port);
			OutputStream out = socket.getOutputStream();
			this.printer = new PrintWriter(out, true);
			InputStream in = socket.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(in);
			this.reader = new BufferedReader(inputStreamReader);
		} catch (IOException e) {
			throw new ConnectionErrorException("Connexion error.");
		}
	}

Afin de pouvoir ouvrir un canal vers un canal pour récuperer les données du serveur, on envoie une commande PASV au serveur, et la réponse qui nous ait
renvoyer et on va ensuite extraire l'adresse ip avec la méthode ipData() et calculer le port a l'aide de la méthode portData().

public ConnectionData commandPasv() throws ConnectionErrorException {
		String commandIpPort = command("PASV");
		int dataPort = ConnectionData.portData(commandIpPort);
		String dataIp = ConnectionData.ipData(commandIpPort);
		return new ConnectionData(dataIp, dataPort);
	}

On récupére le type et nom du fichier d'une ligne d'information fourni par le serveur. On va donc casser la chaîne de caractéres en morceaux a l'aide des
espaces comme séparateur. Si le type du fichier est un lien, on va l'afficher
différement des autres fichiers. On retourne ainsi un tableau contenant le type
et nom d'un fichier.

public static String[] gatherData(String line) {
		String[] splittedLine = line.split(" ");
		String type = Character.toString(splittedLine[0].charAt(0));
		String file = "";
		if (type.equals("l")) {
			file = splittedLine[splittedLine.length - 3];
			file += " -> ";
		}
		file += splittedLine[splittedLine.length - 1];
		String[] data = { type, file };
		return data;
	}

Cette méthode permet de récupérer toute les données contenu dans un dossier pour cela on a besoin du canal de donnée qui va permettre de récupérer les 
informations. On va boucler sur chaque ligne que nous envoie le serveur pour
extraire les informations concernant chaque fichier. On retourne ainsi une liste de ces informations.

private static ArrayList<String[]> gatherDirectoryData(ConnectionData connectionData)
			throws ConnectionErrorException {
		try {
			ArrayList<String[]> listData = new ArrayList<String[]>();
			String line = connectionData.getReader().readLine();
			while (line != null) {
				String[] data = gatherData(line);
				listData.add(data);
				line = connectionData.getReader().readLine();
			}
			return listData;
		} catch (IOException e) {
			throw new ConnectionErrorException("Error gathering data.");
		}
	}

A l'aide d'une connexion et de l'indentation actuelle, profondeur actuelle, et le fichier, on va pouvoir changer de dossier, on regarde d'abord si on a pas
atteint la profondeur limite que l'on souhaiter atteindre, puis on tente de
changer de dossier (!message.contains("Failed")), si cela fonctionne on recommence le processus d'arborescence pour le nouveau dossier. 

private static void switchDirectory(ConnectionCommand connectionCmd, int indentation, int depth, String[] file) {
		if (indentation < depth || depth == -1) {
			try {
				// Il y a un pour moi avec ces 2 dossiers, si je tente d'entré dans ces dossiers, ma connexion s'interrompt
				// instantanément, alors que d'autre dossiers ayant les même droits n'ont pas ce problème.
				// J'ai donc décider de les ignorer.
				if (!(file[1].equals("tmp") || file[1].equals("assistance"))) {
					String message = connectionCmd.command("CWD " + file[1]);
					if (!(message.contains("Failed"))) {
						tree(connectionCmd, indentation + 1, depth);
						connectionCmd.command("CDUP");
					}
				}
			} catch (ConnectionErrorException e) {
				System.out.println("Erreur connexion.");
			} catch (CloseConnectionErrorException e) {
				System.out.println("Erreur pour la fermeture de la connexion");
			}
		}
	}
